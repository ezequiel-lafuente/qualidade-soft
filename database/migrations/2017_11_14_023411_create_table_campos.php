<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCampos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pagina_id')->unsigned()->nullable();
            $table->foreign('pagina_id')->references('id')->on('paginas');
            $table->string('campo');
            $table->integer('video_id')->unsigned()->nullable();
            $table->foreign('video_id')->references('id')->on('videos');
            $table->timestamps();
        });


        DB::table('campos')->insert(array(
            'pagina_id' => 1,
            'campo' => 'INTERFACE_USUARIO'
        ));


        DB::table('campos')->insert(array(
            'pagina_id' => 1,
            'campo' => 'ITENS_NUMERO_SERIE'
        ));

        DB::table('campos')->insert(array(
            'pagina_id' => 1,
            'campo' => 'MONTANTE_IMPRIMIR'
        ));

        DB::table('campos')->insert(array(
            'pagina_id' => 1,
            'campo' => 'NUMERO_MAC_PEDIDOS'
        ));

        DB::table('campos')->insert(array(
            'pagina_id' => 1,
            'campo' => 'GAVETA_DINHEIRO'
        ));

        DB::table('campos')->insert(array(
            'pagina_id' => 1,
            'campo' => 'POLITICA_REEMBOLCO'
        ));

        DB::table('campos')->insert(array(
            'pagina_id' => 1,
            'campo' => 'CREDITO_LOJA'
        ));

        DB::table('campos')->insert(array(
            'pagina_id' => 1,
            'campo' => 'MENSAGEM_RODAPE_RECIBO_1'
        ));

        DB::table('campos')->insert(array(
            'pagina_id' => 1,
            'campo' => 'MENSAGEM_RODAPE_RECIBO_2'
        ));

        DB::table('campos')->insert(array(
            'pagina_id' => 1,
            'campo' => 'REGISTRO_PONTO'
        ));

        DB::table('campos')->insert(array(
            'pagina_id' => 1,
            'campo' => 'HABILITAR_ESTORNO'
        ));

        DB::table('campos')->insert(array(
            'pagina_id' => 1,
            'campo' => 'IMPRIMIR_RECIBO_COZINHA'
        ));

        DB::table('campos')->insert(array(
            'pagina_id' => 1,
            'campo' => 'IMPRIMIR_RECIBO_ORDEM'
        ));

        DB::table('campos')->insert(array(
            'pagina_id' => 1,
            'campo' => 'IMPRIMIR_RECIBO_PEDIDO_PADRAO'
        ));

        DB::table('campos')->insert(array(
            'pagina_id' => 1,
            'campo' => 'IMPRIMIR_RECIBO_NOTA_FISCAL'
        ));

        DB::table('campos')->insert(array(
            'pagina_id' => 1,
            'campo' => 'ENVIAR_RECIBOS'
        ));

        DB::table('campos')->insert(array(
            'pagina_id' => 1,
            'campo' => 'HABILITAR_TELA_FINALIZACAO'
        ));

        DB::table('campos')->insert(array(
            'pagina_id' => 1,
            'campo' => 'CONTROLE_COMISSOES'
        ));

        DB::table('campos')->insert(array(
            'pagina_id' => 1,
            'campo' => 'MAXIMO_HISTORICO'
        ));

        DB::table('campos')->insert(array(
            'pagina_id' => 1,
            'campo' => 'HABILITAR_NFE'
        ));

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campos');
    }
}
