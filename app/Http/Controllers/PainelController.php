<?php

namespace App\Http\Controllers;

use App\Models\Cabo;
use App\Models\Cpu;
use App\Models\Hd;
use App\Models\Linha;
use App\Models\Memoria;
use App\Models\Monitor;
use App\Models\Mouse;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PainelController extends Controller
{


    /**
     * ESTE M�TODO RETORNA A VIEW DO USU�RIOS COM TODOS OS CAMPOS
     */

    public function index()
    {
        return view('painel.formulario_parametros');
    }

    public function save(Request $request)
    {

       $cabo = new Cabo();
       $cabo->marca = !empty($request->get('marca_cabo')) ? $request->get('marca_cabo') : null;
       $cabo->modelo = !empty($request->get('modelo_cabo')) ? $request->get('modelo_cabo') : null;
       $cabo->LINHA_ID = !empty($request->get('linha')) ? $request->get('linha') : null;
       $cabo->save();

       $cpu = new Cpu();
       $cpu->marca = !empty($request->get('marca_cpu')) ? $request->get('marca_cpu') : null;
       $cpu->modelo = !empty($request->get('modelo_cpu')) ? $request->get('modelo_cpu') : null;
       $cpu->numero_patrimoio = !empty($request->get('patrimonio_cpu')) ? $request->get('patrimonio_cpu') : null;
       $cpu->LINHA_ID = !empty($request->get('linha')) ? $request->get('linha') : null;
       $cpu->save();


        $hd = new Hd();
        $hd->marca = !empty($request->get('marca_hd')) ? $request->get('marca_hd') : null;
        $hd->LINHA_ID = !empty($request->get('linha')) ? $request->get('linha') : null;
        $hd->save();

        $mouse = new Mouse();
        $mouse->modelo = !empty($request->get('modelo_mouse')) ? $request->get('modelo_mouse') : null;
        $mouse->cor = !empty($request->get('cor_mouse')) ? $request->get('cor_mouse') : null;
        $mouse->LINHA_ID = !empty($request->get('linha')) ? $request->get('linha') : null;
        $mouse->save();

        $monitor = new Monitor();
        $monitor->marca = !empty($request->get('marca_monitor')) ? $request->get('marca_monitor') : null;
        $monitor->modelo = !empty($request->get('modelo_monitor')) ? $request->get('modelo_monitor') : null;
        $monitor->cor = !empty($request->get('cor_monitor')) ? $request->get('cor_monitor') : null;
        $monitor->numero_patrimoio = !empty($request->get('patrimonio_monitor')) ? $request->get('patrimonio_monitor') : null;
        $monitor->LINHA_ID = !empty($request->get('linha')) ? $request->get('linha') : null;
        $monitor->save();

        $memoria = new Memoria();
        $memoria->marca = !empty($request->get('marca_mem')) ? $request->get('marca_mem') : null;
        $memoria->LINHA_ID = !empty($request->get('linha')) ? $request->get('linha') : null;
        $memoria->save();


        return redirect(url('/list'));

    }

    public function listagem(){
        return view('painel.list');
    }

}
