<?php
/**
 * Created by PhpStorm.
 * User: Ezequiel
 * Time: 13:03
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Linha extends Model
{
    public $table = 'linha';

    public $timestamps = false;
    /**
     * Variaveis seguras para uso e guardar dados
     * @var array
     */
    public $fillable = [
        'id',
        'marca',
        'modelo',
        'voltagem',
        'numero_patrimoio'
    ];
}