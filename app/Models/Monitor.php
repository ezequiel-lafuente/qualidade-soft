<?php
/**
 * Created by PhpStorm.
 * User: Ezequiel
 * Time: 13:03
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Monitor extends Model
{
    public $table = 'monitor';

    public $timestamps = false;

    /**
     * Variaveis seguras para uso e guardar dados
     * @var array
     */
    public $fillable = [
        'id',
        'marca',
        'modelo',
        'polegadas',
        'cor',
        'voltagem',
        'numero_patrimonio'
    ];

}