<?php
/**
 * Created by PhpStorm.
 * User: Ezequiel
 * Time: 13:03
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Memoria extends Model
{
    public $table = 'memoria_ram';

    public $timestamps = false;

    /**
     * Variaveis seguras para uso e guardar dados
     * @var array
     */
    public $fillable = [
        'id',
        'marca'
    ];

}