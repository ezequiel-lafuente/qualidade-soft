<?php
/**
 * Created by PhpStorm.
 * User: Ezequiel
 * Time: 13:03
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hd extends Model
{
    public $table = 'hd';
    /**
     * Variaveis seguras para uso e guardar dados
     * @var array
     */


    public $timestamps = false;

    public $fillable = [
        'id',
        'marca',
        'gigabyte'
    ];


}