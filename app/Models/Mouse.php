<?php
/**
 * Created by PhpStorm.
 * User: Ezequiel
 * Time: 13:03
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mouse extends Model
{
    public $table = 'mouse';

    public $timestamps = false;

    /**
     * Variaveis seguras para uso e guardar dados
     * @var array
     */
    public $fillable = [
        'id',
        'marca',
        'modelo',
        'cor'
    ];

}