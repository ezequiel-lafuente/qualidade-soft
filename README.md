    
# Trabalho Qualidade de Software

      Para esta solução foi utilizada a framework Laravel 5.5, última versão disponível no mercado, onde por sua vez utiliza o padrão MVC. Foi também utilizado uma biblioteca externa para fazer integração com o Youtube,"Youtube Data API v3 (Non-OAuth)", também foi desenvolvido no final os testes unitários, com PHPUnit, abaixo uma demostração de como executá-los.

INICIALIZAÇÃO DO SISTEMA:
     Requisitos: PHP 7.0 ou superior

1 PASSO (Gerênciador de dependências):

    Após clonar o projeto, o primeiro passo é baixar as depências externas que erguem a fremework,
    para isso é preciso ter instalado o Composer.

    link de download => https://getcomposer.org/

2 PASSO (Gerênciador de dependências):

    Na segunda etapa, já presumindo que o composer foi instalado é preciso acessar a raiz do projeto,
    e em seguida através do seu bash executar o seguinte comando:

    command=> "composer install"

    Isso ira baixar suas dependências

3 PASSO (Configuração de Banco de Dados):

    Depois de ja ter baixado as dependências, o trabalho com o composer acabou por enquanto, agora é preciso
    configurar o Banco de Dados. Para isso identifique na raiz do projeto um arquivo chamdo ".env",
    então nesse arquivo ficaram todas variaveis de ambiente da aplicação, e lá estão as variaveis de configuração do banco de dados.

    Exemplo:

    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=sustentare
    DB_USERNAME=root
    DB_PASSWORD=123456

    Configure essas variaveis de acordo com suas informação, certifique-se que realmente existe uma base chamada "sustentare" por exemplo.


4 PASSO (Onde fica o arquivo index.php ?):

    Então o arquivo index.php fica dentro do diretório public, que por sua esta localizado na raiz do projeto,
    então certifique-se de apontar seu virtualhost para essa pasta.
    
    

5 PASSO (Testes Unitários - PHPUnit)

    O seguinte comando deve ser executado na raiz do projeto:

    vendor\bin\phpunit
    










    


