jQuery(document).ready(function () {

    jQuery("[name='cabo']").change(function() {
        jQuery(".cabos").prop("disabled",false);
    });

    jQuery("[name='cpu']").change(function() {
        jQuery(".cpus").prop("disabled",false);
    });

    jQuery("[name='hd_cpu']").change(function() {
        jQuery(".hds").prop("disabled",false);
    });

    jQuery("[name='mouse']").change(function() {
        jQuery(".mouses").prop("disabled",false);
    });

    jQuery("[name='monitor']").change(function() {
        jQuery(".monitores").prop("disabled",false);
    });

    jQuery("[name='memoria']").change(function() {
        jQuery(".mem").prop("disabled",false);
    });

    jQuery("[name='teclado']").change(function() {
        jQuery(".tec").prop("disabled",false);
    });

    jQuery(document).on('click', '.etapa_1', avancarEtapa1);
    jQuery(document).on('click', '.etapa_2', avancarEtapa2);
    jQuery(document).on('click', '.etapa_3', avancarEtapa3);



    jQuery("[name='entidade']").change(function() {
        var value = jQuery(this).val();
        jQuery('.save').show();

        if(value == 'unidade'){
            $('.uni').show();
            $('.con').hide();
            window.scrollBy(0,180);
        } else {
            $('.con').show();
            $('.uni').hide();
            window.scrollBy(0,180);
        }

    });

});

function avancarEtapa1(){
    bloqueiaTela();
    setTimeout(function(){

        jQuery('.etp_2').show();
        window.scrollBy(0,180);

        liberaTela();
    }, 3000);
}

function avancarEtapa2(){
    bloqueiaTela();
    setTimeout(function(){

        jQuery('.etp_3').show();
        window.scrollBy(0,180);

        liberaTela();
    }, 3000);
}

function avancarEtapa3(){
    bloqueiaTela();
    setTimeout(function(){

        jQuery('.etp_4').show();
        window.scrollBy(0,180);

        liberaTela();
    }, 3000);
}
