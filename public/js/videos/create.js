jQuery(document).ready(function () {
    jQuery(document).on('click', '.upload', getVideoByYoutube);
    jQuery(document).on('click', '.remover', removeVideo);
    jQuery(document).on('click', '.adicionar', saveVideoGallery);
});


function getVideoByYoutube() {

    var link = jQuery('.link').val();
    var html = '';
    var bool = validateLink(link);

    if(!bool){
        alert('Link inválido!');
        return false;
    }

    if (link === '') {
        alert('Você precisa colar um link na combo!');
        return false;
    }

    jQuery(".upload , .link").prop("disabled", true);

    bloqueiaTela();

    jQuery.ajax({
        type: "GET",
        url: '/videos-getInfoVideo',
        dataType: "json",
        data: {
            link_youtube: link
        },
        success: function (rs) {

            liberaTela();

            if (rs.success) {
                jQuery(".titulo").text(rs.objeto.titulo);
                jQuery(".descricao").text(rs.objeto.descricao);
                jQuery(".espaco_player").html(rs.objeto.player);
                jQuery(".pre_visualizacao").show();

                for (i = 0; i < rs.objeto.tags.length; i++) {
                    html += '<span class="tag label label-info">' + rs.objeto.tags[i] + '</span> ';
                }

                jQuery(".tags").html(html);
            }
        }
    });

}

function saveVideoGallery() {
    bloqueiaTela();

    jQuery(".upload , .link").prop("disabled", false);

    var link = jQuery('.link').val();

    jQuery.ajax({
        type: "GET",
        url: '/videos-save',
        dataType: "json",
        data: {
            link_youtube: link
        },
        success: function () {
            liberaTela();
            window.location = "/videos";
        }
    });
}

function removeVideo() {
    bloqueiaTela();
    jQuery(".pre_visualizacao").hide();
    jQuery(".upload , .link").prop("disabled", false);
    liberaTela();
}

function validateLink(link){
    if (link != undefined || link != '') {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
        var match = link.match(regExp);
        if (match && match[2].length == 11) {
              return true;
        } else {
            return false;
        }
    }
}
