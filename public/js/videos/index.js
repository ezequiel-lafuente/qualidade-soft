/**
 * Created by ezequ on 12/11/2017.
 */


jQuery(document).ready(function () {
    jQuery(document).on('click', '.video', vincularVideo);
});


function vincularVideo() {
    var video = $(this).attr('data-video-id');
    var campo = $(this).attr('data-campo-id');

    bloqueiaTela();

    jQuery.ajax({
        type: "GET",
        url: '/gerenciador-vincular-video',
        dataType: "json",
        data: {
            campo_id: campo,
            video_id: video
        },
        success: function (rs) {

            if (rs.success) {
                liberaTela();
                alert(rs.msg);
                window.location = "/gerenciador";
            }
        }
    });

}
