@extends('layout.master')

@section('javascript')
    <script src="../../../js/formulario_usuario_sistema.js"></script>
@stop

@section('conteudo')
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Listagem Sustentare</a>
                </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body">
                    <table class="table ls-table">
                        <thead>
                        <tr>
                            <th class="ls-nowrap col-xs-4">Entidade</th>
                            <th class="ls-nowrap col-xs-4">Estado</th>
                            <th class="ls-nowrap col-xs-4">Data de inclusão</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td>
                                Entidade teste 1
                            </td>
                            <td>
                                Inseríveis
                            </td>
                            <td>
                                Não definida
                            </td>
                        </tr>


                        <tr>
                            <td>
                                Entidade teste 2
                            </td>
                            <td>
                                Inseríveis
                            </td>
                            <td>
                                Não definida
                            </td>
                        </tr>


                        <tr>
                            <td>
                                Entidade teste 3
                            </td>
                            <td>
                                Ociosos
                            </td>
                            <td>
                                Não definida
                            </td>

                        </tr>
                        <tr>
                            <td>
                                Entidade teste 4
                            </td>
                            <td>
                                Recuperáveis
                            </td>
                            <td>
                                Não definida
                            </td>


                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
