@extends('layout.master')

@section('javascript')
    <script src="../../../js/formulario_usuario_sistema.js"></script>
@stop

@section('conteudo')
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Cadastro Sustentare</a>
                </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body">

                    <form action="{{url('/salvar')}}" method="POST">
                        {{ csrf_field() }}

                        <div class="form-group">


                            <div class="col-md-12">

                                <h3>Cadastro volume</h3>

                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Cabos </b></label>

                                        <input type="text" name="cabo" class="form-control">

                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Marca</b></label>

                                        <input type="text" name="marca_cabo" class="form-control cabos" disabled>

                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Modelo</b></label>

                                        <input class="form-control cabos" type="text" name="modelo_cabo"
                                               disabled>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>CPU </b></label>

                                        <input type="text" name="cpu" class="form-control cpu">

                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Marca</b></label>

                                        <input type="text" name="marca_cpu" class="form-control cpus" disabled>

                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Modelo</b></label>

                                        <input class="form-control cpus" type="text" name="modelo_cpu"
                                               disabled>
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Voltagem</b></label>

                                        <input class="form-control cpus" type="text" name="voltagem_cpu"
                                               disabled>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Numero Patrimônio</b></label>

                                        <input class="form-control cpus" type="text" name="patrimonio_cpu"
                                               disabled>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>HD </b></label>

                                        <input type="text" name="hd_cpu" class="form-control">

                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Marca</b></label>

                                        <input type="text" name="marca_hd" class="form-control hds" disabled>

                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Gigabyte</b></label>

                                        <input class="form-control hds" type="text" name="giga_hd"
                                               disabled>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Mouse </b></label>

                                        <input type="text" name="mouse" class="form-control">

                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Modelo</b></label>

                                        <input type="text" name="modelo_mouse" class="form-control mouses" disabled>

                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Cor</b></label>

                                        <input class="form-control mouses" type="text" name="cor_mouse"
                                               disabled>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Monitor </b></label>

                                        <input type="text" name="monitor" class="form-control mouses">

                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Marca</b></label>

                                        <input type="text" name="marca_monitor" class="form-control monitores" disabled>

                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Modelo</b></label>

                                        <input class="form-control monitores" type="text" name="modelo_monitor"
                                               disabled>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Polegadas</b></label>

                                        <input class="form-control monitores" type="text" name="polegadas_monitor"
                                               disabled>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Cor</b></label>

                                        <input class="form-control monitores" type="text" name="cor_monitor"
                                               disabled>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Voltagem</b></label>

                                        <input class="form-control monitores" type="text" name="voltagem_monitor"
                                               disabled>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Numero Patrimônio</b></label>

                                        <input class="form-control monitores" type="text" name="patrimonio_monitor"
                                               disabled>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Memória </b></label>

                                        <input type="text" name="memoria" class="form-control mem">

                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Marca</b></label>

                                        <input type="text" name="marca_mem" class="form-control mem" disabled>

                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Gigabyte</b></label>

                                        <input class="form-control mem" type="text" name="giga_mem"
                                               disabled>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Teclado </b></label>

                                        <input type="text" name="teclado" class="form-control tec">

                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Modelo</b></label>

                                        <input type="text" name="modelo_tec" class="form-control tec" disabled>

                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Cor</b></label>

                                        <input class="form-control tec" type="text" name="cor_tec"
                                               disabled>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-body"><br>
                                        <button type="button" class="btn btn-secondary etapa_1">Próxima etapa</button>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-12 etp_2" style="display: none;">

                                <h3>Cadastro de Linha</h3>

                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Linha </b></label>

                                        <select name="linha" class="form-control">
                                            <option value="1">BRANCO</option>
                                            <option value="2">MARROM</option>
                                            <option value="3">VERDE</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-body"><br>
                                        <button type="button" class="btn btn-secondary etapa_2">Próxima etapa</button>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-12 etp_3" style="display: none;">

                                <h3>Estado do Processo</h3>

                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Estado </b></label>

                                        <select name="estado" class="form-control">
                                            <option>Ociosos outros</option>
                                            <option>Ociosos completos</option>
                                            <option>Inseríveis</option>
                                            <option>Recuperáveis</option>
                                        </select>

                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-body"><br>
                                        <button type="button" class="btn btn-secondary etapa_3">Próxima etapa</button>
                                    </div>
                                </div>

                            </div>


                            <div class="col-md-12 etp_4" style="display: none;">

                                <h3>Entidade Responsável</h3>

                                <div class="col-md-4">
                                    <div class="form-body">
                                        <label><b>Tipo </b></label>

                                        <select name="entidade" class="form-control">
                                            <option value="conveniado">Conveniado</option>
                                            <option value="unidade">Unidade Administrativa</option>
                                        </select>

                                    </div>
                                </div>

                                <div class="col-md-4 con" style="display: none">
                                    <div class="form-body">
                                        <label><b>Nome</b></label>

                                        <input type="text" name="modelo_tec" class="form-control con">

                                    </div>
                                </div>

                                <div class="col-md-4 con" style="display: none">
                                    <div class="form-body">
                                        <label><b>Telefone</b></label>

                                        <input type="text" name="modelo_tec" class="form-control con">

                                    </div>
                                </div>

                                <div class="col-md-4 con" style="display: none">
                                    <div class="form-body">
                                        <label><b>CNPJ</b></label>

                                        <input type="text" name="modelo_tec" class="form-control con">

                                    </div>
                                </div>

                                <div class="col-md-4 con" style="display: none">
                                    <div class="form-body">
                                        <label><b>Região</b></label>

                                        <input type="text" name="modelo_tec" class="form-control con">

                                    </div>
                                </div>

                                <div class="col-md-4 uni" style="display: none">
                                    <div class="form-body">
                                        <label><b>Cidade</b></label>

                                        <input type="text" name="modelo_tec" class="form-control uni">

                                    </div>
                                </div>

                                <div class="col-md-4 uni" style="display: none">
                                    <div class="form-body">
                                        <label><b>Rua</b></label>

                                        <input type="text" name="modelo_tec" class="form-control uni">

                                    </div>
                                </div>

                                <div class="col-md-4 uni" style="display: none">
                                    <div class="form-body">
                                        <label><b>Bairro</b></label>

                                        <input type="text" name="modelo_tec" class="form-control uni">

                                    </div>
                                </div>

                                <div class="col-md-4 uni" style="display: none">
                                    <div class="form-body">
                                        <label><b>CEP</b></label>

                                        <input type="text" name="modelo_tec" class="form-control uni">

                                    </div>
                                </div>

                                <div class="col-md-4 uni" style="display: none">
                                    <div class="form-body">
                                        <label><b>Referencia</b></label>

                                        <input type="text" name="modelo_tec" class="form-control uni">

                                    </div>
                                </div>
                                <br>

                                <div class="col-md-12 save" style="display: none">
                                    <div class="form-body">
                                        <button type="submit" class="btn btn-secondary save">Salvar</button>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
