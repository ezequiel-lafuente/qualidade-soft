@extends('layout.master')

@section('javascript')
    <script src="../../../js/formulario_usuario_sistema.js"></script>
@stop

@section('conteudo')
    <div class="container-fluid pb-video-container">
        <div class="col-md-10 col-md-offset-1">
            <h3 class="text-center">Galeria de videos
                <button type="button" class="btn btn-primary"> <span class="glyphicon glyphicon-plus"></span> Novo vídeo</button>
            </h3>
            <div class="row pb-row">
                <div class="col-md-3 pb-video">
                    <iframe class="pb-video-frame" width="100%" height="230" src="https://www.youtube.com/embed/K68UrdUOr2Y?list=RDzuAcaBkcYGE?ecver=1" frameborder="0" allowfullscreen></iframe>
                    <label class="form-control label-info text-center" data-id="1">Ver mais <span class="glyphicon glyphicon-search"></span></label>
                </div>
            </div>
        </div>
    </div>

    <style>

    </style>
@endsection